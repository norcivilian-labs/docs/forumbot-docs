# forumbot-docs

Documentation for the forumbot project, built with [mdBook](https://github.com/rust-lang/mdBook) and hosted at [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
